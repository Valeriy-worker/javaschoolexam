package com.tsystems.javaschool.tasks.calculator;

import java.util.Objects;

/**
 * Класс представляет операнд или оператор с приоритетом вычислений.
 */
class Element implements Comparable<Element> {
    /**
     * Элемент отображение операнда/оператора типа String
     */
    private String element;
    /**
     * Отображение приоритета типа int
     */

    private int priority;

    Element(String element) {
        this(element, 0);
    }

    Element(String element, int priority) {
        this.element = element;
        this.priority = priority;
    }

    String getElement() {
        return element;
    }

    int getPriority() {
        return priority;
    }

    void setPriority(int priority) {
        this.priority = priority;
    }

    boolean isDigit() {
        return element.matches("\\-*\\d+\\.*\\d*");
    }

    double getDigit() {
        return Double.valueOf(element);
    }

    @Override
    public int compareTo(Element other) {
        return priority - other.priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Element element = (Element) o;
        return priority == element.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(priority);
    }
}
