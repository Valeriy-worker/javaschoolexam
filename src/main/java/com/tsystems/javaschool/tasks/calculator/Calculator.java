package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Calculator {

    private String statement;
    private List<Element> list = new LinkedList<>();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        this.statement = statement;
        if (!check()) return null;
        getCollection();
        setPriority();
        calculate();
        String result = getResult();
        return result;
    }

    private String getResult() {
        double res = (!list.isEmpty() ? list.get(0).getDigit() : 0);
        String result = (res - (int) res) == 0 ? String.valueOf((int) res) : String.valueOf(res);
        return result;
    }

    private boolean check() {
        if (statement == null || statement.isEmpty()) return false;

        //Проверка на последовательность скобок
        int indexOpenBracket = statement.indexOf('(');
        int indexCloseBracket = statement.indexOf(')');
        boolean bracketsOrder = indexOpenBracket <= indexCloseBracket;

        //Проверка на совпадение количества '(' и ')'
        int OpenBracketCount = statement.split("[(]").length - 1;
        int CloseBracketCount = statement.split("[)]").length - 1;
        boolean bracketsCount = OpenBracketCount == CloseBracketCount;

        //Проверка на недопустимый символ
        Pattern symbolsPattern = Pattern.compile("[^0-9\\.\\+\\-\\*\\/ ()]");
        boolean symbols = !symbolsPattern.matcher(statement).find();

        //Проверка на отсутствие двух операторов подряд
        Pattern operatorPattern = Pattern.compile("[\\+\\-\\*\\/]{2,}");
        boolean operator = !operatorPattern.matcher(statement).find();

        //Проверка на отсутствие двух и более "." в одном числе
        //Проверка на наличие целой части числа
        Pattern dotPattern = Pattern.compile("(\\.+\\d+\\.+)|(\\.{2,})|(\\D\\.)");
        boolean dot = !dotPattern.matcher(statement).find();

        /*
        Проверка на наличие наличие оператора перед "(" и после ")",
        Проверка на  отсутствие оператора перед ")",
        */
        Pattern bracketPattern = Pattern.compile("\\d\\(|\\)\\d|[\\+\\-\\*\\/]\\)");
        boolean bracket = !bracketPattern.matcher(statement).find();

        return operator && dot && bracket && bracketsOrder && bracketsCount && symbols;
    }

    private void calculate() throws ArithmeticException {
        deleteBrackets();
        while (list.size() > 1) {
            //Element с максимальным приоритетом
            Element max = list.stream()
                    .max(Element::compareTo)
                    .get();
            int index = list.indexOf(max);
            String buf = operate(index);
            int leftPriority = index > 0 ? list.get(index - 1).getPriority() : 0;
            int rightPriority = index < list.size() - 1 ? list.get(index + 1).getPriority() : 0;
            int priority = Math.min(leftPriority, rightPriority) - 1;
            list.set(index, new Element(buf, priority));
            if (index < list.size() - 1 && list.get(index + 1).isDigit()) list.remove(index + 1);
            if (index > 0 && list.get(index - 1).isDigit()) list.remove(index - 1);
        }
    }

    private String operate(int index) throws ArithmeticException {
        double res = 0;
        double a = (index > 0 && list.get(index - 1).isDigit()) ? list.get(index - 1).getDigit() : 0;
        double b = (index < list.size() - 1 && list.get(index + 1).isDigit()) ? list.get(index + 1).getDigit() : 0;
        switch (list.get(index).getElement()) {
            case "+":
                res = a + b;
                break;
            case "-":
                res = a - b;
                break;
            case "*":
                res = a * b;
                break;
            case "/":
                res = a / b;
        }
        if (Double.isInfinite(res) || Double.isNaN(res)) throw new ArithmeticException(String.valueOf(res));
        return String.valueOf(res);
    }

    private void deleteBrackets() {
        list = list.stream()
                .filter(s -> !s.getElement().equals("("))
                .filter(s -> !s.getElement().equals(")"))
                .collect(Collectors.toList());
    }

    private void setPriority() {
        int priority = 1;
        int minPriority = 0;
        //Расстановка приоритетов операций для операторов и скобок
        for (int i = 0; i < list.size(); i++) {
            Element e = list.get(i);
            if (e.getElement().matches("\\d+\\.*+\\d*")) e.setPriority(priority - 1);
            if (e.getElement().matches("\\+|\\-")) e.setPriority(priority);
            if (e.getElement().matches("\\*|\\/")) e.setPriority(priority + 1);
            if (e.getElement().matches("\\(")) e.setPriority(priority += 2);
            if (e.getElement().matches("\\)")) e.setPriority(priority -= 2);
            minPriority = Math.min(minPriority, priority);
        }
        //Расстановка приоритетов операций для чисел, приоритет которых должен быть наименьшим
        for (int i = 0; i < list.size(); i++) {
            Element e = list.get(i);
            if (e.getElement().matches("\\d+\\.*+\\d*")) e.setPriority(minPriority - 1);
        }
    }

    private void getCollection() {
        String[] arr = statement.split("");
        for (int i = 0; i < arr.length; ) {
            boolean flag = true;
            StringBuilder sb = new StringBuilder();

            while (i < arr.length && arr[i].matches("\\d|\\.")) {
                sb.append(arr[i++]);
                flag = false;
            }
            if (flag) sb.append(arr[i++]);
            list.add(new Element(sb.toString()));
        }
    }
}
