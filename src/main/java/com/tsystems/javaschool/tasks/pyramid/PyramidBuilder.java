package com.tsystems.javaschool.tasks.pyramid;

import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    private List<Integer> inputNumbers;
    private int rows;
    private int columns;
    private int[][] pyramid;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        this.inputNumbers = inputNumbers;
        if (!check()) throw new CannotBuildPyramidException();
        inputNumbers.sort(Integer::compareTo);
        pyramid = new int[rows][columns];
        setValues();
        return pyramid;
    }

    private void setValues() {
        int start = 0;
        int end = 0;
        int middle = columns / 2;
        Iterator<Integer> iterator = inputNumbers.iterator();
        for (int row = 0; row < rows; row++) {
            start = middle - row;
            end = middle + row;
            for (int column = start; column <= end; column += 2) {
                if (iterator.hasNext()) pyramid[row][column] = iterator.next();
            }
        }
    }

    private boolean check() {
        if (inputNumbers.contains(null)) {
            return false;
        }
        int size = inputNumbers.size();
        int numbersCount = 0;
        /**
         * пирамиду можно построить если в колеекции 3,6,10,15 ... k(k+1)/2 элементов
         * количество столбцов определяется по мат.прогрессии {@code columns = 1+2(rows-1)}
         * */
        for (int i = 1; i <= i * (i + 1) / 2; i++) {
            numbersCount = i * (i + 1) / 2;
            if (numbersCount == size) {
                rows = i;
                columns = 1 + 2 * (i - 1);
                return true;
            }
        }
        return false;
    }
}
